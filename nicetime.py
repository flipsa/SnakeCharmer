# Set the time from ntp
def setNTPTime():
    import ntptime
    ntptime.settime()
    #ntptime.time()    # Print time as epoch

def screenInfo():
    import screenOutput
    import utime

    year = str(utime.localtime()[0])
    month = str(utime.localtime()[1])
    if len(month) == 1:
        month = "0" + month
    day = str(utime.localtime()[2])
    if len(day) == 1:
        day = "0" + day
    hour = str(utime.localtime()[3])
    if len(hour) == 1:
        hour = "0" + hour
    minute = str(utime.localtime()[4])
    if len(minute) == 1:
        minute = "0" + minute
    second = str(utime.localtime()[5])
    if len(second) == 1:
        second = "0" + second
    niceTime = year + "." + month + "." + day + " " + hour + ":" + minute # + ":" + second
    screenOutput.screenPrint(niceTime,0,0)
