def connect(wifiMode):
    import network
    import time
    import nicetime
    import screenOutput

    ssid      = "Wifi Name"
    password  =  "Wifi Password"

    ip        = '192.168.1.123'
    subnet    = '255.255.255.0'
    gateway   = '192.168.1.1'
    dns       = '192.168.1.1'

    sta_if = network.WLAN(network.STA_IF)

    if sta_if.isconnected() == True:
        print("Already connected")
        return

    sta_if.active(True)

    # Comment out next line for static IP
    if wifiMode == "static":
        sta_if.ifconfig((ip,subnet,gateway,dns))

    sta_if.connect(ssid,password)

    while sta_if.isconnected() == False:
        print("Not yet connected...")
        time.sleep(1)
        pass

    print("Connection successful")
    print(sta_if.ifconfig())
    #screenInfo()
    #ntptime.settime()
    nicetime.setNTPTime()

def disconnect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    sta_if.disconnect()
    sta_if.active(False)

def screenInfo():
    import network
    import screenOutput

    ip = network.WLAN(network.STA_IF).ifconfig()[0]
    mask = network.WLAN(network.STA_IF).ifconfig()[1]
    gw = network.WLAN(network.STA_IF).ifconfig()[2]
    dns = network.WLAN(network.STA_IF).ifconfig()[3]

    #screenOutput.screenFill(0)
    #screenOutput.screenPrint(utime.localtime(),0,0)
    screenOutput.screenPrint(ip,0,20)
    screenOutput.screenPrint(mask,0,30)
    screenOutput.screenPrint(gw,0,40)
    screenOutput.screenPrint(dns,0,50)
