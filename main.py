import os
import _thread
import time
import utime
import nicetime
import noDebug
import wifi
import screenOutput

#no_debug()

# Connect to wifi; wifiMode must be either "dhcp" or "static"
wifiMode = "static"
wifi.connect(wifiMode)
#time.sleep(5)

def status():
    while True:

        screenOutput.screenFill(0)
        nicetime.screenInfo()
        wifi.screenInfo()
        screenOutput.screenShow()
        time.sleep(0.5)

_thread.start_new_thread(testThread, ())
