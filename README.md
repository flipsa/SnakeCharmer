# SnakeCharmer

This is a minimal app to get started with Micropython on the TTGO development board with ESP32 (WiFi & Bluetooth), battery (holder & charging circuit), and a SSD1306 display (see pictures below)

The app currently does the following:
- connects to the network specified in wifi.py 
- gets the current time from a NTP server
- shows the time and wifi info on the display (in a separate thread!)
- can be accessed with REPL or webREPL

Requirements:
- [esptool.py](https://github.com/espressif/esptool) 
- [MicroPython firmware for ESP32 boards](https://micropython.org/download#esp32)
- [SSD1306 driver](https://raw.githubusercontent.com/micropython/micropython/master/drivers/display/ssd1306.py)
- [DevBoard (e.g. from [BangGood](https://www.banggood.com/LILYGO-TTGO-ESP32-WiFi-bluetooth-18650-Battery-Protection-Board-0_96-Inch-OLED-Development-Tool-p-1213497.html))

![Board](images/board.jpg)
![Pinout](images/pinout.png)

