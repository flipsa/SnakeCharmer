import machine, ssd1306
i2c = machine.I2C(scl=machine.Pin(4), sda=machine.Pin(5))
oled = ssd1306.SSD1306_I2C(128, 64, i2c, 60)

def screenFill(color):
    if color == 0:
        oled.fill(0)
    elif color == 1:
        oled.fill(1)

def screenPrint(screenString,pos,line):
    oled.text(screenString,pos,line)

def screenShow():
    oled.show()
